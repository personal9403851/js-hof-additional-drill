const got = require('./data-1')

const peopleNameOfAllHouses = () => {

    let namesByHouses = {}

    Object.keys(got).forEach(key => {

        got[key].forEach((obj) => {
            namesByHouses[obj.name] = []

            obj.people.forEach((person) => {
                namesByHouses[obj.name].push(person.name);
            })
        })
    });

    return namesByHouses;
}

console.log(peopleNameOfAllHouses())