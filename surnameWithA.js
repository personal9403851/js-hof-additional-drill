const got = require('./data-1')

const surnameWithA = () => {

    let arrayOfNames = [];

    Object.keys(got).forEach((key) => {

        got[key].forEach((obj) => {

            let fullName = []

            obj.people.forEach(person => {

                fullName = person.name.split(" ")

                if (fullName[fullName.length - 1].startsWith("A"))
                    arrayOfNames.push(person.name)
            });
        })
    })
    return arrayOfNames
}

console.log(surnameWithA())