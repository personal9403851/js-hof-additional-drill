const got = require('./data-1')

const namesWithS = () => {

    let arrayOfNames = [];

    Object.keys(got).forEach((key) => {

        got[key].forEach((obj) => {

            obj.people.forEach(person => {
                if (person.name.startsWith("S"))
                    arrayOfNames.push(person.name)
            });
        })
    })
    return arrayOfNames
}

console.log(namesWithS())