const got = require('./data-1')

const namesIncludeAanda = () => {

    let arrayOfNames = [];

    Object.keys(got).forEach((key) => {

        got[key].forEach((obj) => {

            obj.people.forEach(person => {

                let nameOfPerson = person.name.toLowerCase();
                if (nameOfPerson.includes("s"))
                    arrayOfNames.push(person.name)
            });
        })
    })
    return arrayOfNames
}

console.log(namesIncludeAanda())